package com.madmobile.pandora.services;

import java.util.List;

import com.madmobile.pandora.dto.D365Dto;
import com.madmobile.pandora.dto.KWIDto;

public interface PandoraService {
	
	public List<KWIDto> getAllKWIProduct();


	public List<D365Dto> getAllD365DtoProduct();
	
}
