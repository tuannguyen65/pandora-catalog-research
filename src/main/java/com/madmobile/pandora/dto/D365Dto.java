package com.madmobile.pandora.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public class D365Dto {
	
	Long id;
	String description;

}
