package com.madmobile.pandora.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public class KWIDto {
	
	Long id;
	String description;

}
