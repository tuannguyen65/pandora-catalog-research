package com.madmobile.pandora.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.madmobile.pandora.dto.KWIDto;


@Repository("kwiRepository")
public interface KwiRepository extends JpaRepository<KWIDto, Long> {

}
