package com.madmobile.pandora.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.madmobile.pandora.dto.D365Dto;

public interface d365Repository extends JpaRepository<D365Dto, Long>{

}
